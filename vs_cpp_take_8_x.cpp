#include <iostream>
#include "Dog.h"
#include "Cat.h"
#include "Mouse.h"
#include "Cow.h"

int main()
{
    Dog* dog = new Dog();
    Cat* cat = new Cat();
    Cow* cow = new Cow();
    Mouse* mouse = new Mouse();

    Animal* animals[4]{ dog, cat, cow, mouse };

    for (Animal* a : animals)
    {
        a->Voice();
    }
}