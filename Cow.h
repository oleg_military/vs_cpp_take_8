#pragma once
#include "Animal.h"

class Cow :public Animal
{
	void Voice() override
	{
		std::cout << "Cow say: Moo!" << std::endl;
	}
};